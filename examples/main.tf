module "redis_cache" {
  source = "../"

  project = var.project
  region  = var.region

  name           = "test-redis"
  tier           = "STANDARD_HA"
  memory_size_gb = 2

  labels = {
    terraform = "ok"
  }

}
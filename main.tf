resource "google_redis_instance" "cache" {
  name           = var.name
  tier           = var.tier
  project        = var.project
  region         = var.region
  memory_size_gb = var.memory_size_gb

  authorized_network = var.authorized_network

  redis_version = var.redis_version

  labels = var.labels
}
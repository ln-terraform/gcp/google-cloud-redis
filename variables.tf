variable "project" {
  type = string
}

variable "region" {
  type = string
}

variable "name" {
  type = string
}

variable "authorized_network" {
  type    = string
  default = "default"
}

variable "memory_size_gb" {
  type    = number
  default = 1
}

variable "redis_version" {
  type    = string
  default = "REDIS_5_0"
}

variable "tier" {
  type    = string
  default = "BASIC"
}

variable "labels" {
  type    = map(string)
  default = {}
}



